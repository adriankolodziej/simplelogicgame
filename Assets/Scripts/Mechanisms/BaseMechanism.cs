﻿using UnityEngine;

namespace Assets.Scripts.Mechanisms
{
	public abstract class BaseMechanism : MonoBehaviour
	{
		public abstract void PerformAction();
	}
}